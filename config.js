// This is client side config only - don't put anything in here that shouldn't be public!
export const endpoint = process.env.NEXT_PUBLIC_BACKEND_API;
export const perPage = 4;
